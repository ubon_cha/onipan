package oniPan;

/**童謡おにのパンツを表示させるプログラム
 * @author kyuta
 *
 */
public class Main {

	/**メインメソッド
	 * @param args
	 */
	public static void main(String[] args) {
		execute();
	}

	/**oniPanを歌う処理
	 *
	 */
	public static void execute() {
		String lyric = "";

		lyric += sayOniPan();
		lyric += sayIiPan();
		lyric += newLine();
		lyric += repeatMessage(sayTuyoizo(), 2);
		lyric += newLine();
		lyric += sayOniKegawa();
		lyric += sayDekiteiru();
		lyric += newLine();
		lyric += repeatMessage(sayTuyoizo(), 2);
		lyric += newLine();
		lyric += sayNumNen(5);
		lyric += sayHaitemo();
		lyric += sayYaburenai();
		lyric += newLine();
		lyric += repeatMessage(sayTuyoizo(), 2);
		lyric += newLine();
		lyric += sayNumNen(10);
		lyric += sayHaitemo();
		lyric += sayYaburenai();
		lyric += newLine();
		lyric += repeatMessage(sayTuyoizo(), 2);
		lyric += newLine();
		lyric += repeatMessage(sayHakou(), 2);
		lyric += sayOniPan();
		lyric += newLine();
		lyric += repeatMessage(sayHakou(), 2);
		lyric += sayOniPan();
		lyric += newLine();
		lyric += repeatMessage(sayAnatamo(), 4);
		lyric += newLine();
		lyric += sayMinnnade();
		lyric += sayHakou();
		lyric += sayOniPan();

		System.out.println(lyric);

	}

	/**同じメッセージを複数回表示
	 * @param msg
	 * @param count
	 * @return
	 */
	public static String repeatMessage(String msg, int count){
		String repeatMsg = "";
		for(int i = 0; i < count; i++ ){
			repeatMsg += msg;
		}
		return repeatMsg;
	}

	public static String newLine(){
		return "\n";
	}

	/**強いぞを表示
	 * @return
	 */
	public static String sayTuyoizo(){
		return "強いぞ";
	}

	/**はこうを表示
	 * @return
	 */
	public static String sayHakou(){
		return "はこう";
	}

	/**鬼のパンツを表示
	 * @return
	 */
	public static String sayOniPan(){
		return "鬼のパンツ";
	}

	/**はいてもを表示
	 * @return
	 */
	public static String sayHaitemo(){
		return "はいても";
	}

	/**やぶれないを表示
	 * @return
	 */
	public static String sayYaburenai(){
		return "やぶれない";
	}

	/**鬼の毛皮を表示
	 * @return
	 */
	public static String sayOniKegawa(){
		return "鬼の毛皮で";
	}

	/**できているを表示
	 * @return
	 */
	public static String sayDekiteiru(){
		return "できている";
	}

	/**いいパンツを表示
	 * @return
	 */
	public static String sayIiPan(){
		return "いいパンツ";
	}

	/**num + 年を表示
	 * @param num
	 * @return
	 */
	public static String sayNumNen(int num){
		return num + "年";
	}

	/**あなたもを表示
	 * @return
	 */
	public static String sayAnatamo(){
		return "あなたも";
	}

	public static String sayMinnnade(){
		return "みんなで";
	}
}
